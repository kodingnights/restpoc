# Quick Spring Boot REST PoC #

Just a quick test to test some capabilities of Spring Boot, REST, Swagger, Autodocs

### How do I get set up? ###

Clone. Run. Go to http://localhost:5000

A copy is deployed to AWS here: http://springbootpoc.eu-central-1.elasticbeanstalk.com/

Find the source on Bitbucket: https://bitbucket.org/kodingnights/restpoc

### Who do I talk to? ###

* DKoding
