package no.dkit.twone;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static no.dkit.twone.Controller.HELLO_ENDPOINT;
import static org.hamcrest.Matchers.is;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = ControllerTest.Config.class)
public class ControllerTest {
    public static final String YOU = "A";
    public static final String ME = "B";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    protected ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
            .alwaysDo(MockMvcRestDocumentation.document("{class-name}/{method-name}",
                Preprocessors.preprocessRequest(),
                Preprocessors.preprocessResponse(
                    ResponseModifyingPreprocessors.replaceBinaryContent(),
                    ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
                    prettyPrint())))
            .apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation)
                .uris()
                .withScheme("http")
                .withHost("localhost")
                .withPort(8080)
                .and().snippets()
                .withDefaults(CliDocumentation.curlRequest(),
                    HttpDocumentation.httpRequest(),
                    HttpDocumentation.httpResponse(),
                    AutoDocumentation.requestFields(),
                    AutoDocumentation.responseFields(),
                    AutoDocumentation.pathParameters(),
                    AutoDocumentation.requestParameters(),
                    AutoDocumentation.description(),
                    AutoDocumentation.methodAndPath(),
                    AutoDocumentation.section()))
            .build();
    }

    @Test
    public void hello() throws Exception {
        mockMvc.perform(
            RestDocumentationRequestBuilders
                .get(Controller.ROOT_URL + HELLO_ENDPOINT + "{you}/{me}", YOU, ME))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.you").value(is(YOU)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.me").value(is(ME)));
    }

    @TestConfiguration
    @EnableWebMvc
    public static class Config {

    }
}
