package no.dkit.twone;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Controller.ROOT_URL)
@Slf4j
@Api(value = "/", description = "Hello controller operations")
@ApiResponses(value = {
    @ApiResponse(code = 200, message = "OK", response = Service.Hello.class),
    @ApiResponse(code = 401, message = "Unauthorized", response = Void.class),
    @ApiResponse(code = 403, message = "Forbidden", response = Void.class),
    @ApiResponse(code = 404, message = "Not Found", response = Void.class)})
public class Controller {
    public static final String ROOT_URL = "/";
    public static final String HELLO_ENDPOINT = "hello/";

    private final Service service;

    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    /**
     * Say hello to me
     *
     * @param you Who are you?
     * @param me And who am I?
     * @return The reply and {@link HttpStatus} OK
     */
    @GetMapping(value = HELLO_ENDPOINT + "{you}/{me}", produces = "application/json")
    public
    @ResponseBody
    ResponseEntity<Service.Hello> hello(
        @ApiParam(value = "Who you are", required = true) @PathVariable("you") String you,
        @ApiParam(value = "Who I am", required = true) @PathVariable("me") String me) {
        return new ResponseEntity<>(service.hello(you, me), HttpStatus.OK);
    }
}
