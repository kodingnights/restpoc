package no.dkit.twone;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service {
    Dao dao;

    @Autowired
    public Service(Dao dao) {
        this.dao = dao;
    }

    public Hello hello(String you, String me) {
        return new Hello(you, me);
    }

    @Data
    @AllArgsConstructor
    public static final class Hello {
        String you;
        String me;
    }
}
